# todo have an autonmatic creation if value is left empty
variable "name" {
  type    = "string"
  default = ""
}

variable "ami_id" {
  type = "string"
}

variable instance_type {
  type    = "string"
  default = "t2.micro"
}

variable "instance_profile_name" {
  type    = "string"
  default = ""
}

variable "instance_security_groups" {
  type    = "list"
  default = []
}

variable "instance_target_group_arns" {
  type    = "list"
  default = []
}

variable "subnet_ids" {
  type = "list"
}

variable "vpc_id" {
  type = "string"
}

variable "dynamodb_registry_name" {
  type    = "string"
  default = "services"
}

variable "key_name" {
  type    = "string"
  default = ""
}

variable "lb_security_groups" {
  type    = "list"
  default = []
}

variable "service_linked_role_arn" {
  type = "string"
}

variable "logs_bucket" {
  type = "string"
}
