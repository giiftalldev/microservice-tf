resource "random_id" "server" {
  byte_length = 3
}

locals {
  name = "${var.name == "" ? random_id.server.hex : var.name}"
}

resource "aws_lb" "microservice" {
  name               = "microservice-${local.name}"
  internal           = true
  load_balancer_type = "application"

  subnets      = ["${var.subnet_ids}"]
  idle_timeout = 120

  enable_cross_zone_load_balancing = true
  security_groups                  = ["${var.lb_security_groups}"]

  access_logs {
    bucket  = "${var.logs_bucket}"
    prefix  = "${local.name}"
    enabled = true
  }
}

resource "aws_lb_target_group" "microservice" {
  name_prefix = "${local.name}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${var.vpc_id}"
}

resource "aws_lb_listener" "microservice" {
  load_balancer_arn = "${aws_lb.microservice.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.microservice.arn}"
    type             = "forward"
  }
}

module "asg" {
  source                     = "git::https://bitbucket.org/giiftalldev/autoscalling-group-tf.git"
  name                       = "${local.name}"
  ami_id                     = "${var.ami_id}"
  subnet_ids                 = "${var.subnet_ids}"
  instance_target_group_arns = ["${aws_lb_target_group.microservice.arn}"]
  instance_security_groups   = ["${var.instance_security_groups}"]
  instance_profile_name      = "${var.instance_profile_name}"
  key_name                   = "${var.key_name}"
  service_linked_role_arn    = "${var.service_linked_role_arn}"
}

data "aws_dynamodb_table" "db" {
  name = "${var.dynamodb_registry_name}"
}

resource "aws_dynamodb_table_item" "declaration" {
  table_name = "${var.dynamodb_registry_name}"
  hash_key   = "${data.aws_dynamodb_table.db.hash_key}"

  item = <<ITEM
{
  "key": {"S": "${local.name}"},
  "address": {"S": "http://${aws_lb.microservice.dns_name}"}
}
ITEM
}
