# Microservice deploying template

## Usage
```tf
module "service" {
    source = "bitbucket.org/giiftalldev/microservice-tf.git"
    name = "test"
    subnet_ids = ["${data.aws_subnet_ids.subnets.ids}"]
    vpc_id = "${data.aws_vpc.selected.id}"
    ami_id = "${data.aws_ami.ubuntu.image_id}"
}
```